<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Pulso extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, Accept, Authorization, X-Requested-With, Application,application/x-www-form-urlencoded, multipart/form-data, text/plain");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }
  public function index_post()
  {
      $data= $this->post();
      //comprobar si todas las variables estan llenas
    if($data['idU']!="" AND $data['pulso']!="" AND $data['podometro']!="")
    {

              if( isset( $data['idU'] ) AND isset( $data['pulso'] ) AND isset( $data['podometro'] ))
              {
                
                   $this->db->reset_query();
                   $insertar_mediciones = array('idUsuario'=>$data['idU'], 'pulso'=>$data['pulso'],'Podometro'=>$data['podometro']);
                   $hecho=$this->db->insert('medicion', $insertar_mediciones );

                    $respuesta =array('error'=> FALSE,
                                     'mensaje'=>'Usuario registrado correctamente');
                    $this->response($respuesta);
              }else
              {
                $respuesta =array('error'=> FALSE,
                                     'mensaje'=>'malo');
                    $this->response($respuesta);
                   return;
              }
    
    }
  }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Actualizar extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, Accept, Authorization, X-Requested-With, Application,application/x-www-form-urlencoded, multipart/form-data, text/plain");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }

  public function index_post()
  {
      $data= $this->post();
      $anoNac="";
      $year="";
      //comprobar si todas las variables estan llenas
    if(!isset( $data['nombre'] ) OR !isset( $data['apellidoP'] ) OR !isset( $data['apellidoM']) OR !isset($data['fechaN']) OR !isset($data['numeroT']) OR !isset($data['id']) )
    {
          $respuesta= array('error'=> TRUE, 'mensaje'=>'La información enviada no es válida');
          $this->response($respuesta, REST_Controller::HTTP_BAD_REQUEST );
          return;
    }

    $year=date('Y');
    $dd=date('d');
    $mm=date('m');
    $anoMin=$year-120;
    $fechaMin=$anoMin."-".$mm."-".$dd;
    $anoMax=$year-18;
    $fechaMax=$anoMax."-".$mm."-".$dd;

    if( $data['nombre']!="" AND $data['apellidoP']!="" AND $data['apellidoM']!="" AND $data['fechaN']!="" AND $data['numeroT']!="" AND $data['id'] )
    {
      if( $data['fechaN']<=$fechaMax && $data['fechaN']>=$fechaMin )
      {
            if( isset( $data['nombre'] ) AND isset( $data['apellidoP'] ) AND isset( $data['apellidoM']) AND isset($data['fechaN']) AND isset($data['numeroT']) AND isset($data['id'])  )
            {
              $nacimiento= date($data['fechaN']);          
               $condiciones= array('Id'=>$data['id'] );
               $query= $this->db->get_where('usuario',$condiciones );
                $usuario= $query->row();
               $this->db->reset_query();
               $actualizar_perfil = array('Nombre'=>$data['nombre'], 'Apellido_paterno'=>$data['apellidoP'],'Apellido_materno'=>$data['apellidoM'],'Fecha_nacimiento'=>$nacimiento ,'Numero_telefono'=> $data['numeroT']);
               $hecho=$this->db->update('usuario', $actualizar_perfil,array('Id'=>$data['id']));
               $respuesta =array('error'=> FALSE,
                                 'mensaje'=>'Usuario registrado correctamente');
               $this->response($respuesta);
                   return;
              
            }
        }
        else
        {
          $respuesta =array('error'=> TRUE,
            'mensaje'=>'La fecha maxima son 120 años y la minima 18, porfavor ingrese una fecha valida ');

             $this->response($respuesta);
               return;
        }

      }
      else
      {

            $respuesta =array('error'=> TRUE,
            'mensaje'=>'Los campos no pueden quedar vacios ');

             $this->response($respuesta);
               return;
      }

    }
  }

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Usuario extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }

  public function index_get($id)
  {

      if($id!="")
      {
        $query= $this->db->query("SELECT * FROM usuario WHERE Id=".$id);
        $usuario= $query->row();
        
        if( !isset($usuario))
        {
          $respuesta = array('error' => FALSE,
          'mensaje' =>"No existe un usuario con esta sesion");
           $this->response($respuesta, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
          $respuesta = array('error' => FALSE,
          'usuario' => $query->result_array());
           $this->response($respuesta);
        }
      }
      else
      {
        $respuesta =array('error' => TRUE,
                         'mensaje'=>"No se ha encontrado una sesion activa");
        $this->response($respuesta, REST_Controller::HTTP_UNAUTHORIZED );                 
      }
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Recuperacion extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }

  public function index_post()
  {
    $data= $this->post();

    if( !isset( $data['correo'] ))
    {
      $respuesta= array('error'=> TRUE,
                        'mensaje'=>'La información enviada no es válida'
                       );
      $this->response($respuesta, REST_Controller::HTTP_BAD_REQUEST );
      return;
    }

    //Tenemos correo y contraseña en un post
    $condiciones= array('Correo'=>$data['correo']);

    $query= $this->db->get_where('usuario',$condiciones );
    $usuario= $query->row();

    if( !isset( $usuario ) )
    {
      $respuesta= array('error'=>TRUE,
                        'mensaje'=>'Este correo no esta registrado en nuestra base de datos');
      $this->response( $respuesta);
      return;
    }
    else
    {
    //hola 
    //Aqui, tenemos usuario y contraseña validos
    //Guardar en base de datos el Token
    $user =array(
            'id' => $usuario->Id,
            'nombre'=>$usuario->Nombre
        );
    
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'saviorwatch@gmail.com', // change it to yours
        'smtp_pass' => 'SAviorwatch2018', // change it to yours
        'charset' => 'iso-8859-1',
        'mailtype' => 'html',
        'wordwrap' => TRUE
        );
                    
            $message = "Hola,".$user['nombre'].". <br><br> Se ha solicitado un cambio de clave a tu cuenta de SaviorWatch. <br><br>  porfavor has click en el siguiente enlace: <a href='http://192.168.1.20/SaviorWeb/recuperacion.php?id=".$user['id']."'>Click Aqui</a> <br><br> Si no has solicitado cambiar tu clave, puedes ignorar este correo electronico y no se cambiara tu clave.";
             $this->load->library('email', $config);
             $this->email->set_newline("\r\n");
             $this->email->from('saviorwatchgmail.com','Administrador SaviorWatch'); // change it to yours
             $this->email->to($data['correo']);// change it to yours
             $this->email->subject('Resume from JobsBuddy for your Job posting');
             $this->email->message($message);
             if($this->email->send())
            {
                 $respuesta =array('error'=> FALSE,
                                    'mensaje'=>'Hemos enviado un correo a su direccion');              

                 $this->response($respuesta);
                 return;
            }
         else
        {
            $respuesta = array('error'=> TRUE,
                                'mensaje'=> 'No se ha podido realizar el envio del correo');
                                
            $this->response($respuesta,REST_Controller::HTTP_BAD_REQUEST);
        }
    }

  }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Login extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }

  public function index_post()
  {
      $data= $this->post();

      if( !isset( $data['correo'] ) OR !isset($data['clave']))
      {
        $respuesta= array('error'=> TRUE,
                          'mensaje'=>'La información enviada no es válida'
                         );
        $this->response($respuesta, REST_Controller::HTTP_BAD_REQUEST );
        return;
      }

      //Tenemos correo y contraseña en un post
      $condiciones= array('Correo'=>$data['correo'],
                          'Clave'=>$data['clave'] );

      $query= $this->db->get_where('usuario',$condiciones );
      $usuario= $query->row();

      if( !isset( $usuario ) )
      {
        $respuesta= array('error'=>TRUE,
                          'mensaje'=>'Usuario/contrasena no son validos');
        $this->response( $respuesta);
        return;
      }
      //hola 
      //Aqui, tenemos usuario y contraseña validos
      //Guardar en base de datos el Token

      $respuesta =array('error'=> FALSE,
                        'token'=>$usuario->Token,
                        'id'=>$usuario->Id,
                        'clave'=>$usuario->Clave,
                        'nombre_usuario'=>$usuario->Nombre_usuario,
                        'correo'=>$usuario->Correo,
                        'clave'=>$usuario->Clave,
                        'perfil'=>$query->result_array()
                      );

      $this->response($respuesta);
  }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Singup extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, Accept, Authorization, X-Requested-With, Application,application/x-www-form-urlencoded, multipart/form-data, text/plain");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }
  public function index_post()
  {
      $data= $this->post();
      //comprobar si todas las variables estan llenas
    if(!isset( $data['usuario'] ) OR !isset( $data['correo'] ) OR !isset( $data['clave'] ))
    {
          $respuesta= array('error'=> TRUE, 'mensaje'=>'La información enviada no es válida'.$data['clave'].$data['correo'].$data['usuario']);
          $this->response($respuesta, REST_Controller::HTTP_BAD_REQUEST );
          return;
    }
    if($data['usuario']!="" AND $data['correo']!="" AND $data['clave']!="")
    {


            if(filter_var($data['correo'], FILTER_VALIDATE_EMAIL))
            {
              if( isset( $data['usuario'] ) AND isset( $data['correo'] ) AND isset( $data['clave'] ))
              {
                $condiciones= array('Correo'=>$data['correo'] );
                $query= $this->db->get_where('usuario',$condiciones );
                $usuario= $query->row();
                //Validar que no hayan usuarios iguales
                if(!isset( $usuario ))
                {
                  $condiciones2= array('Nombre_usuario'=>$data['usuario']);
                  $query2=$this->db->get_where('usuario',$condiciones2);
                  $usuario2=$query2->row();
             
                 if(isset( $usuario2 ))
                  {
                    $respuesta= array('error'=>TRUE, 'mensaje'=>'Este nombre de usuario ya tiene registro en nuestro sistema');
                    $this->response( $respuesta);
                    return;
                 }
                  else
                  {
                   //Se cambio rut por correo 
                   //Aqui, tenemos un usuario inexistente
                   //crear Token
                   $token = bin2hex(openssl_random_pseudo_bytes(20) );
                   $token= hash('ripemd160', $data['correo'] );
                   $tipoUser=1;
                   $this->db->reset_query();
                   $actualizar_token = array('Nombre_usuario'=>$data['usuario'], 'Correo'=>$data['correo'],'Clave'=>$data['clave'],'Tipo_usuario_fk'=>$tipoUser ,'token'=> $token );
                   $hecho=$this->db->insert('usuario', $actualizar_token );

                    $respuesta =array('error'=> FALSE,
                                     'mensaje'=>'Usuario registrado correctamente');
                    $this->response($respuesta);
                   return;
                  }
               }
                else
                {
                 $respuesta= array('error'=>TRUE, 'mensaje'=>'ya existe este correo en nuestro sistema');
                 $this->response( $respuesta);
                 return;
               }
             }
           }
           else
           {
              $respuesta= array('error'=>TRUE, 'mensaje'=>'Correo invalido, porfavor ingrese un correo valido');
             $this->response($respuesta);
              return;
           } 
         }
         else
         {

                $respuesta =array('error'=> TRUE,
                'mensaje'=>'Los campos no pueden quedar vacios');

                 $this->response($respuesta);
                   return;
         }



  }
}

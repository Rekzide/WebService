<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Mediciones extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }

  public function index_get($id)
  {

      if($id!="")
      {
        $query= $this->db->query("SELECT * FROM medicion WHERE IdUsuario=".$id);
        $mediciones= $query->row();
        
        if( !isset($mediciones))
        {
          $respuesta = array('error' => FALSE,
          'mensaje' =>"No existen mediciones de este usuario");
           $this->response($respuesta, REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
          $respuesta = array('error' => FALSE,
          'medicion' => $query->result_array());
           $this->response($respuesta);
        }
      }
      else
      {
        $respuesta =array('error' => TRUE,
                         'mensaje'=>"No se ha encontrado una sesion activa");
        $this->response($respuesta, REST_Controller::HTTP_UNAUTHORIZED );                 
      }
    }
}
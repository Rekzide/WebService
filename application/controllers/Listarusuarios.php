<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'./libraries/REST_Controller.php');
use Restserver\libraries\REST_Controller;

class Listarusuarios extends REST_Controller
{
  public function __construct()
  {
    header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
    header("Access-Control-Allow-Origin: *");

    parent::__construct();
    $this->load->database();
  }

  public function index_get()
  {
    $query= $this->db->query('SELECT * FROM usuario');

    $respuesta = array('error'=> FALSE, 'usuario'=>$query->result_array());

    $this->response($respuesta);
  }

  public function borrar_usuario_delete( $token="0", $id="0")
  {
    if($token == "0" || $id == "0")
    {
      $respuesta= array(
                'error' => TRUE,
                'mensaje' => "Token invalido y/o usuario invalido.");
      $this->response($respuesta, REST_Controller::HTTP_BAD_REQUEST);
      return;
    }

    $condiciones = array('Id'=> $id, 'Token'=>$token);
    $this->db->where($condiciones);
    $existe=$query->row();

    if(!$existe)
    {
      $respuesta =array('error'=>TRUE,
                        'mensaje'=>"Usuario y Token incorrectos");
      $this->response($respuesta);
      return;
    }

    //verificar si la orden es del usuario
    $this->db->reset_query();
    $condiciones = array('Id'=>$id, 'Token'=>$token);
    $this->db->where($condiciones);
    $query = $this->db->get('ordenes');

    $existe =$query->row();
    if(!$existe)
    {
      $respuesta =array(
                  'error'=>TRUE,
                  'mensaje'=>"Este usuario no se puede eliminar");
      $this->response($respuesta);
      return;
    }

    //Todo bien
    $condiciones =array('Id'=>$id);
    $this->db->delete('usuario', $condiciones);

    $respuesta= array('error'=>FALSE,
                      'mensaje'=>"Usuario Eliminado");

    $this->response($respuesta);
  }

}
